#!/bin/sh
set -e

SRCDIR=$(dirname "$0")
[ -z "$DESTDIR" ] && DESTDIR=/usr

# Install .desktop files
install -vdm0755 "${DESTDIR}/share/applications"
find "${SRCDIR}/applications" -name "*.desktop" \
    -exec install -vDm0644 '{}' "${DESTDIR}/share/applications" \;

# Install pixmaps
install -vdm0755 "${DESTDIR}/share/pixmaps"
find "${SRCDIR}/pixmaps" -name "*-app.svg" \
    -exec install -vDm0644 '{}' "${DESTDIR}/share/pixmaps" \;

# Install wrapper script
install -vdm0755 "${DESTDIR}/bin"
install -vDm0755 "${SRCDIR}/chromium-persistant-webapp" "${DESTDIR}/bin"

echo "All done."
