#!/bin/sh
set -e

SRCDIR=$(dirname "$0")
[ -z "$DESTDIR" ] && DESTDIR=/usr

# Uninstall .desktop files
find "${SRCDIR}/applications" -name "*.desktop"  \
    -printf "${DESTDIR}/share/applications/%f\n" \
    | xargs -n1 rm -vf

# Uninstall pixmaps
find "${SRCDIR}/pixmaps" -name "*-app.svg"      \
    -printf "${DESTDIR}/share/pixmaps/%f\n"     \
    | xargs -n1 rm -vf

# Uninstall wrapper script
rm -vf "${DESTDIR}/bin/chromium-persistant-webapp"

echo "All done."
